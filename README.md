# AutoScripts

Simple macro scripts for every day use. To be used in conjuction with BTT, or
any other tool that can execute scripts.

### 8to4.py

Types 8:00 AM, tabs to next input and types 4:00 PM

Shortcut: `Ctrl+Alt+8`


### 9to5.py

Types 9:00 AM, tabs to next input and types 5:00 PM

Shortcut: `Ctrl+Alt+9`
