# !/usr/bin/env python3
from time import sleep
import pyautogui
from dotenv import dotenv_values


# load .env values directly
config = dotenv_values('.env')

# navigate bananatag login form
pyautogui.typewrite(list(config['USER']) + ['tab'] + ['enter'])
sleep(1)
pyautogui.typewrite(list(config['PASS']))
