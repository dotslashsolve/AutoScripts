# !/usr/bin/env python3

'''markdown
# `8to4` : keyboard macro

Set up a macro to automatically type a string on the keyboard. This script
specifically types "8:00 am \tab\tab4:00 pm" for daily timetracking on
[toggl](https://toggl.com/).

## Installation
This script requires Python 3.x for the libraries. To install on OSX:
```bash
brew install python3
```

To install the OSX specific dependancies libraries gobally (good enough for our
puposes):
```bash
sudo pip3 install pyobjc-framework-quartz pyobjc-core pyobjc pyautogui
```

## Add Shortcut (Better Touch Tool)
Using BetterTouchTool we can add a keyboard shortcut. Under 'keyboard' create a
new 'shortcut' using whatever keys you want (I used Ctrl+Shift+8). Under
'Assigned action' use 'controlling other applications > Execute terminal command
(Async)'. In the popup enter:
```bash
/usr/local/bin/python3 ~/path_to_script/8to4.py
```

I'd recommend setting the macro as a context sensitive shortcut (ie: only works
when chrome is the 'active' app).

## Reference
[dependancies](https://automatetheboringstuff.com/chapter18/#calibre_link-3621)
[sending keystrokes](https://automatetheboringstuff.com/chapter18/#calibre_link-3668)
'''
import pyautogui

pyautogui.typewrite(['8', ':', '0', '0'])
